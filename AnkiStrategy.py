﻿# -*- coding: UTF-8 -*-
# This file is part of AnkiStrategy. Copyright: 2013 <tcp-ip_80@libero.it>
# License: GNU AGPL, version 3 or later.
# This program is intended to be an add-on for Anki 2,
# note that Anki is copyrighted by Damien Elmes, who releases it under the same licence.
# You should have received a copy of the AGPL along with Anki. If not, see http://www.gnu.org/licenses/agpl.html

from ankistrategymain import AnkiStrategy

ast = AnkiStrategy()
