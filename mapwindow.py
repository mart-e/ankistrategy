# -*- coding: UTF-8 -*-
# This file is part of AnkiStrategy. Copyright: 2013 <tcp-ip_80@libero.it>
# License: GNU AGPL, version 3 or later.
# This program is intended to be an add-on for Anki 2,
# note that Anki is copyrighted by Damien Elmes,
# who releases it under the same licence.
# You should have received a copy of the AGPL along with Anki.
# If not, see http://www.gnu.org/licenses/agpl.html

from aqt.qt import (
    QDialog,
    QPainter,
    QColor,
    QVBoxLayout,
    QImage,
    QLabel,
    qRgb,
    QProgressBar,
)
from ankistrategy.game import GameWorld, COLORS
from aqt import mw
import threading


class SplashWin(QDialog):
    def __init__(self):
        super(SplashWin, self).__init__()
        self.setGeometry(400, 200, 200, 100)
        self.layout = QVBoxLayout()
        self.setLayout(self.layout)
        self.label = QLabel("drawing map")
        self.layout.addWidget(self.label)
        self.layout.addWidget(QProgressBar(self, minimum=0, maximum=0))
        self.show()


class MapWindow(QDialog):
    def mapCoordinates(self, coordinates, mapSize, worldSize):
        c = [0, 0]
        for i in range(2):
            c[i] = int(mapSize[i] * coordinates[i] / worldSize[i])
        return tuple(c)

    def reverseMapCoordinates(self, coordinates, mapSize, worldSize):
        c = [0.0, 0.0]
        for i in range(2):
            c[i] = worldSize[i] * (coordinates[i] + 0.5) / mapSize[i]
        return tuple(c)

    def cityColor(self, city, x, y):
        # Returns that color with which the point (x, y) in the area of the city must be painted
        topFaction = max([(city.factions[i], i) for i in city.factions])[1]
        if topFaction == None:
            return COLORS[1]
        else:
            return self.world.players[topFaction].color

    def paintCityNames(self, painter, color=None):
        for i in self.pixelsWithCities:
            leader = max(
                [
                    (self.pixelsWithCities[i].factions[x], x)
                    for x in self.pixelsWithCities[i].factions
                ]
            )
            if color == None:
                painter.setPen(QColor(*self.world.players[leader[1]].color))
            else:
                painter.setPen(QColor(*color))
            painter.drawText(i[0], i[1], self.pixelsWithCities[i].name)

    def drawMap(self):
        self.imgReady = threading.Lock()
        self.imgReady.acquire()
        self.pixelsWithCities = {}
        for i in self.world.cities.values():  # mapping cities
            coordinates = self.mapCoordinates(
                i.coordinates, (self.width(), self.height()), self.world.WORLDSIZE
            )
            if (
                coordinates in self.pixelsWithCities
                and pixelWithCities[coordinates].size > i.size
            ):
                continue
            else:
                self.pixelsWithCities[coordinates] = i
        for x in range(0, self.width()):  # drawing pixels
            for y in range(0, self.height()):
                if (x, y) in self.pixelsWithCities:
                    city = self.pixelsWithCities[(x, y)]
                    self.img.setPixel(x, y, qRgb(*COLORS[0]))
                else:
                    positionInWorld = self.reverseMapCoordinates(
                        (x, y), (self.width(), self.height()), self.world.WORLDSIZE
                    )
                    influences = [
                        ((abs(x - c[0]) + abs(y - c[1])) ** 2, self.pixelsWithCities[c])
                        for c in self.pixelsWithCities
                    ]  # TODO meglio usare la distanza nel mondo del gioco
                    controllingCity = sorted(influences, key=lambda city: city[0])[0][1]
                    self.img.setPixel(
                        x, y, qRgb(*self.cityColor(controllingCity, x, y))
                    )
        self.imgReady.release()

    def show(self):
        if not self.imgReady.acquire(False):
            self.sw = SplashWin()
            mw.app.processEvents()
            self.imgReady.acquire(True)
            del self.sw
        super(MapWindow, self).show()
        self.imgReady.release()

    def paintEvent(self, e):
        p = QPainter()
        p.begin(self)
        p.drawImage(0, 0, self.img)
        self.paintCityNames(p, COLORS[0])
        p.end()

    def __init__(self, world):
        super(MapWindow, self).__init__()
        self.world = world
        size = self.world.WORLDSIZE
        maxSize = max(world.WORLDSIZE)
        ratio = 600.0 / maxSize
        self.setGeometry(
            200, 100, world.WORLDSIZE[0] * ratio, world.WORLDSIZE[1] * ratio
        )
        self.img = QImage(self.width(), self.height(), 4)
        self.img.fill(QColor(0, 0, 0))
        self.t = threading.Thread(target=self.drawMap)
        self.t.start()
