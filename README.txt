AnkiStrategy 0.1
A simple strategy game for Anki.

Some of you may know AnkiEmperor and AnkiWarrior, two simple games in which you try to conquer a country, getting a score according to how many cards you review in Anki.
I found them interesting, but too simple. In fact, they are not games, they are just fancy progress bars. 
Instead of a simple number running from 0% to 100%, you progress is counted in terms of cities, inhabitants and monuments.
Yet, they lack competition, you can make virtually no strategical choice and you lose nothing if you don't keep pace with your flashcards.

AnkiStrategy tries to improve his predecessors in the following ways:
1- It has competition: you compete with computer-controlled players to conquer cities.
2- It is also a strategy game: you have to make strategical decisions on how to use the energy you get from your work (although admittedly this is still very basic in this release).
3- Most important thing for its motivation purpose: if you are sloppy in your repetitions you will go backward.

RULES:
REAL WORLD RULES:
You decide the amount of cards you plain to review each day. 
In each turn you get a virtue proportional to the square root of the portion of work you do. For example if you set a 100 cards/day target and you study 10 cards, next day you will have a 31.7% virtue.
If you study 200 cards, you will get a 141% virtue.
I have chosen the square root because this way you are encouraged do every day at least a part of your target (if one day you have had no time, study just 1 card and this will give you a 10% virtue).
Conversely, if you do more that you planned, you are rewarded, but not too much: you are better follow your plains or reconsider them.
If you change your target, the new value will become effective only next day. Thus, you can't procrastinate.
You virtue affects the ability of your player to influence the game world. Furthermore, if you do at least 95% of your work for 1 week you'll get an ambassador.
Computer-controlled players receive on average the same virtue you get if you do 95% of your work.
Hence, if you work diligently, you will have an edge on them.

GAME WORLD RULES:
The game is turn-based, with one turn for each real world day.
Every day you can press "Next turn" to process the daily turn. If you don't use Anki for some days you can press it more times to play all the previous turns. 
The inhabitants of each city are divided into factions supporting a player, some are neutral.
Each turn 10% of the people in each faction (20% of the neutral citizens) becomes undecided and are convinced to join some other factions depending on player's clouts.
The clout of a player in each city depends on several parameters:
1- If he has already got supporters in that city, they will try to convince other people.
2- If he is part of the city government, he can use his power to convince other inhabitants.
3- If he is in the city he can campaign for himself. He can also send an ambassador to campaign for him.
The zeal of your supporters, your governments, your player and your ambassadors are all proportional to the virtue you got in the previous real world day.
Each turn part of the population relocates to other cities, spreading the factions.
You rank will increase from peasant to emperor, as you join the government of more cities.


AnkiStrategy is still very simple, but, if it raises some interest, I may spend some time to improve it.
I encourage you to report bugs (including mistakes in my English, note that I'm not a mother tongue speaker), and propose improvements.
 


 

