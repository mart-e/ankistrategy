# -*- coding: UTF-8 -*-
# This file is part of AnkiStrategy. Copyright: 2013 <tcp-ip_80@libero.it>
# License: GNU AGPL, version 3 or later.
# This program is intended to be an add-on for Anki 2,
# note that Anki is copyrighted by Damien Elmes, who releases it under the same licence.
# You should have received a copy of the AGPL along with Anki. If not, see http://www.gnu.org/licenses/agpl.html

from aqt.webview import AnkiWebView, QVBoxLayout
from PyQt5.QtWidgets import *
from aqt.qt import QWidget, QDialog, QAction, QLineEdit, QLabel
from aqt import mw
import os.path, sys, pickle, datetime
from ankistrategy.game import GameWorld, Player, City, HONORIFICS, COLORS
from ankistrategy.mapwindow import MapWindow
from aqt.utils import showInfo


class SettingsWindow(QDialog):
    def display(self, name, target):
        layout = QVBoxLayout()
        self.setLayout(layout)
        nameBox = QLineEdit(name)
        targetBox = QLineEdit(str(int(target)))
        layout.addWidget(QLabel("Your player's name:"))
        layout.addWidget(nameBox)
        layout.addWidget(QLabel("Your player's target:"))
        layout.addWidget(targetBox)
        self.exec_()
        return nameBox.text(), targetBox.text()

    def __init__(self,):
        super(SettingsWindow, self).__init__()


class AnkiStrategyMainWindow(QWidget):
    def citiesListLinkHandler(self, link):
        link = link.split(":")
        if link[0] == "playermove":
            if self.world.players[self.observingPlayerID].location:
                self.world.ordersForNextTurn.append(
                    (
                        "playermove",
                        self.observingPlayerID,
                        self.world.players[self.observingPlayerID].location.name,
                        link[1],
                    )
                )
            else:  # Special case for the first turn
                self.world.players[self.observingPlayerID].location = self.world.cities[
                    link[1]
                ]
                self.world.cities[link[1]].playersInCity.append(
                    self.world.players[self.observingPlayerID]
                )
        elif link[0] == "ambassadorassigned":
            ambassador = [
                x for x in self.world.newAmbassadors if x[0] == self.observingPlayerID
            ][0]
            self.world.ordersForNextTurn.append(
                ("ambassadorAssigned", ambassador, link[1])
            )
            self.world.newAmbassadors.remove(ambassador)
        self.showCitiesList()
        self.program.saveWorld()

    def showCitiesList(self):
        cities = list(self.world.cities.values())
        cities.sort(key=lambda x: x.size, reverse=True)
        h = []
        for i in cities:
            rulers = [
                self.program.playerFullName(x, True) for x in i.government if x != None
            ]
            if None in i.government:
                rulers.append("local politicians")
            rulers = " ".join(rulers)
            h.append(
                i.name
                + " with "
                + str(i.size)
                + " inhabitants  Ruled by: "
                + rulers
                + "<p>Factions: "
            )
            factions = [(x, i.factions[x]) for x in i.factions if x != None]
            factions.sort(key=lambda x: x[1], reverse=True)
            factions = [
                (self.world.players[x[0]].name + ": " + str(x[1]) + "   ", x[0])
                for x in factions
            ]
            factions = [self.program.HTMLColoredPlayer(x[0], x[1]) for x in factions]
            if None in i.factions:
                factions.append("   neutral: " + str(i.factions[None]))
            h.append(" ".join(factions))
            playersInCity = [
                self.program.HTMLColoredPlayer(x.name, x.id) for x in i.playersInCity
            ]
            if len(playersInCity) > 0:
                playersInCity = " ".join(playersInCity) + "<p>"
                h.append("<p>Players in city: " + playersInCity)
            ambassadors = [
                self.program.HTMLColoredPlayer(self.world.players[x[0]].name, x[0])
                + " until turn "
                + str(x[1])
                for x in i.ambassadors
            ]
            if len(ambassadors) > 0:
                ambassadors = ", ".join(ambassadors)
                h.append("<p>Ambassadors: " + ambassadors)
            if i != self.world.players[self.observingPlayerID].location:
                if (
                    len(
                        [
                            x
                            for x in self.world.filterEvents(
                                "player",
                                self.observingPlayerID,
                                self.world.ordersForNextTurn,
                            )
                            if x[0] == "playermove"
                        ]
                    )
                    == 0
                ):  # player has already moved
                    h.append('<p><a href="playermove:' + i.name + '">come here</a>')
            if len(
                [x for x in self.world.newAmbassadors if x[0] == self.observingPlayerID]
            ):
                h.append(
                    ' <a href="ambassadorassigned:' + i.name + '">send ambassador</a>'
                )
            h.append("<hr>")
        self.lowerFrame.stdHtml("".join(h))
        self.lowerFrame.setLinkHandler(self.citiesListLinkHandler)

    def showPlayersList(self):
        players = list(self.world.players.values())
        players.sort(key=lambda x: x.rank, reverse=True)
        h = []
        for i in players:
            if i.location == None:
                location = "nowhere"
            else:
                location = i.location.name
            if len(i.virtues) > 0:
                virtue = "{:.3f}%".format(100 * i.virtues[-1])
            else:
                virtue = "still to see"
            s = "{} is in {}. Virtue in last turn: {}<hr>".format(
                self.program.playerFullName(i.id, False), location, virtue
            )
            h.append(self.program.HTMLColoredPlayer(s, i.id))
        self.lowerFrame.stdHtml("".join(h))

    def settings(self):
        player = [x for x in self.world.players.values() if not x.isAI][0]
        name = player.name
        changed = False
        target = self.realWorldData["target"]
        sw = SettingsWindow()
        newName, newTarget = sw.display(name, target)
        if newName != name:
            player.name = newName
            changed = True
        try:
            newTarget = float(newTarget)
            if newTarget <= 0:
                raise ValueError
        except ValueError:
            showInfo("invalid value for target")
            return
        if newTarget != target:
            if self.world.turn != 0:
                self.realWorldData["newTarget"] = newTarget
            else:
                self.realWorldData["target"] = newTarget
            changed = True
        else:
            if "newTarget" in self.realWorldData:
                del self.realWorldData["newTarget"]
                changed = True
        if changed == True:
            self.program.saveWorld()
            self.upperFrameInit()

    def translateEvents(self, e):
        def verbNumber(verb, n):
            if verb == "have":
                if n <= 1:
                    return "has"
                else:
                    return "have"
            else:
                if n <= 1:
                    return n + "s"
                else:
                    return n

        s = "WARNING" + str(e)  # dl
        if e[0] == "governmentChange":
            incomingRulers = [
                (self.world.players[x].name, self.world.players[x].id)
                for x in e[2]
                if x != None
            ]
            verbIn = verbNumber("have", len(incomingRulers))
            incomingRulers = [
                self.program.HTMLColoredPlayer(
                    self.program.playerHonorific(x[1]) + " " + x[0], x[1]
                )
                for x in incomingRulers
            ]
            formerRulers = [
                (self.world.players[x].name, self.world.players[x].id)
                for x in e[3]
                if x != None
            ]
            verbOut = verbNumber("have", len(e[3]))
            formerRulers = [
                self.program.HTMLColoredPlayer(
                    self.program.playerHonorific(x[1]) + " " + x[0], x[1]
                )
                for x in formerRulers
            ]
            incomingRulers = ", ".join(incomingRulers)
            formerRulers = ", ".join(formerRulers)
            if incomingRulers != "" and formerRulers != "":
                s = "{} {} joined the government of {} and {} {} been expelled".format(
                    incomingRulers, verbIn, e[1], formerRulers, verbOut
                )
            elif incomingRulers != "":
                # verb = verbNumber('have', len(e[2]))
                s = "{} {} joined the government of {}".format(
                    incomingRulers, verbIn, e[1]
                )
            elif formerRulers != "":
                # verb = verbNumber('have', len(e[3]))
                s = "{} {} been expelled from the government of {}".format(
                    formerRulers, verbOut, e[1]
                )
            else:
                s = ""
        elif e[0] == "playerRankChange":
            name = self.world.players[e[1]].name
            oldHonorific = self.program.honorific(e[2])
            newHonorific = self.program.honorific(e[3])
            if oldHonorific != newHonorific:
                verb = ["promoted", "demoted"][e[2] > e[3]]
                s = "{} {} has been {} to {}".format(
                    oldHonorific, name, verb, newHonorific
                )
            else:
                s = "{} {}'s rank has slightly changed from {}% to {}%".format(
                    newHonorific, name, e[2] / 10.0, e[3] / 10.0
                )
            s = self.program.HTMLColoredPlayer(s, e[1])
        elif e[0] == "newAmbassador":
            s = "A new ambassador is going to support {}".format(
                self.program.playerFullName(e[1][0], True)
            )
        elif e[0] == "noNewAmbassador":
            s = "{} wasn't virtuous enough to get a new ambassador (virtue: {:.3f}%)".format(
                self.program.playerFullName(e[1], True), 100 * e[2]
            )
        elif e[0] == "oldAmbassadorRemoved":
            s = "{}'s ambassador in {} has retired after {} turns".format(
                self.program.playerFullName(e[2][0], True), e[1], e[2][1]
            )
        elif e[0] == "ambassadorAssigned":
            s = "{} has assigned his new ambassador to {}".format(
                self.program.playerFullName(e[1][0], True), e[2]
            )
        elif e[0] == "playermove":
            s = "{} has moved from {} to {}".format(
                self.program.playerFullName(e[1], True), e[2], e[3]
            )
        return s

    def showEventsList(self):

        readableEvents = [self.translateEvents(i) for i in self.world.turnLog]
        # readableOrdersForNextTurn=[self.translateEvents(i) for i in self.world.ordersForNextTurn]
        # s=readableEvents+['<hr>orders for next turn<p>']+readableOrdersForNextTurn
        s = readableEvents
        s = [x for x in s if x != ""]
        self.lowerFrame.stdHtml("<hr>".join(s))

    def showPathToAmbassador(self):
        CUTOFFS = (0.95, 1)

        def warningHTMLColors(x, s, cutOffs):
            if x < cutOffs[0]:
                return '<FONT COLOR="FF0000">' + s + "</FONT>"
            elif x >= cutOffs[1]:
                return '<FONT COLOR="00FF00">' + s + "</FONT>"
            else:
                return '<FONT COLOR="FF8C00">' + s + "</FONT>"

        h = "<b>If you get an average work done of 95% during a week, a new ambassador will support you</b><hr>"
        daysSinceLast = self.world.turn % 7
        lastVirtues = []
        if daysSinceLast > 0:
            lastVirtues = self.world.players[self.observingPlayerID].virtues[
                -daysSinceLast:
            ]
        nextTurnDate = self.realWorldData["startDate"] + datetime.timedelta(
            days=self.world.turn + 1
        )
        todayWork = self.program.getWorkDoneOn(
            (self.program.getAnkiDay() - nextTurnDate + datetime.timedelta(days=1)).days
        )
        lastVirtues.append((todayWork / self.realWorldData["target"]) ** 0.5)
        average = sum(lastVirtues) / len(lastVirtues)
        h += "<hr>".join(
            [
                str(i + 1)
                + ":    "
                + warningHTMLColors(x, "{:.3f}%".format(100 * x), CUTOFFS)
                for i, x in enumerate(lastVirtues)
            ]
        )
        h += "<hr><hr><b>Average:</b>    " + warningHTMLColors(
            average, "{:.3f}%".format(100 * average), CUTOFFS
        )
        self.lowerFrame.stdHtml(h)

    def upperFrameInit(self):
        h = """<a href="showCitesList">Cities</a>
        <a href="showPlayersList">Players</a>
        <a href="showEventsList">Events</a>
        <a href="showPathToAmbassador">New ambassador</a>
        <a href="showMap">display map</a>
        <b><a href="execTurn">Next Turn</a></b>
        <a href="Settings">Settings</a> <br>
        Started on {}, Current turn: {}, Target: {} card/day
        """.format(
            str(self.realWorldData["startDate"]),
            self.world.turn,
            self.realWorldData["target"],
        )
        if "newTarget" in self.realWorldData:
            h += " (New target: {})".format(self.realWorldData["newTarget"])
        self.upperFrame.stdHtml(h)

    def upperFrameLinkHandler(self, link):
        handlers = {
            "showCitesList": self.showCitiesList,
            "showPlayersList": self.showPlayersList,
            "showEventsList": self.showEventsList,
            "showPathToAmbassador": self.showPathToAmbassador,
            "showMap": self.program.mapWindow.show,
            "execTurn": self.program.execTurn,
            "Settings": self.settings,
        }
        r = handlers[link]()
        if r != None:
            if link == "execTurn" and r == False:
                showInfo("Last possible turn has already been done")
            else:
                self.upperFrameInit()
                self.showEventsList()

    def __init__(self, program, observingPlayerID):
        super(AnkiStrategyMainWindow, self).__init__()
        self.program = program  # a link back to the AnkiStrategy object
        self.world = self.program.world
        self.realWorldData = self.program.realWorldData
        self.observingPlayerID = observingPlayerID
        self.setWindowTitle("AnkiStrategy")
        self.__layout = QVBoxLayout()
        self.setLayout(self.__layout)
        self.upperFrame = AnkiWebView()
        self.lowerFrame = AnkiWebView()
        self.__layout.addWidget(self.upperFrame)
        self.__layout.addWidget(self.lowerFrame)
        self.upperFrame.onBridgeCmd = self.upperFrameLinkHandler
        self.upperFrameInit()
        self.show()
        self.upperFrame.setMaximumHeight(self.height() / 5)


class AnkiStrategy(object):
    def saveWorld(self):
        f = open(self.worldPath, "wb")
        pickle.dump(self.world, f, 2)
        pickle.dump(self.realWorldData, f, 2)
        f.close()

    def loadWorld(self):
        f = open(self.worldPath, "rb")
        self.world = pickle.load(f)
        self.realWorldData = pickle.load(f)
        f.close()

    def getWorkDoneOn(self, day):
        dayRange = [
            "id < %d" % ((mw.col.sched.dayCutoff - (day * 86400)) * 1000),
            "id > %d " % ((mw.col.sched.dayCutoff - ((day + 1) * 86400)) * 1000),
        ]
        dayRange = " and ".join(dayRange)
        return mw.col.db.scalar("select count() from revlog where " + dayRange)

    def getAnkiDay(self):
        timeOffset = datetime.datetime.fromtimestamp(mw.col.crt).time()
        ankiDay = datetime.date.today()
        if datetime.datetime.now().time() < timeOffset:
            ankiDay -= datetime.timedelta(days=1)
        return ankiDay

    def execTurn(self):
        # is it time for a new turn?
        if datetime.datetime.now() > datetime.datetime.fromtimestamp(
            mw.col.sched.dayCutoff
        ):
            showInfo("Anki hasn't updated to today yet")
            return False
        ankiDay = self.getAnkiDay()
        nextTurnDate = self.realWorldData["startDate"] + datetime.timedelta(
            days=self.world.turn + 1
        )
        if nextTurnDate > ankiDay:
            return False
        # turn
        workDone = self.getWorkDoneOn(
            (ankiDay - nextTurnDate + datetime.timedelta(days=1)).days
        )
        self.world.execTurn(workDone / self.realWorldData["target"])
        self.world.setNewTurn()
        self.mapWindow = MapWindow(self.world)
        # target change
        if "newTarget" in self.realWorldData:
            self.realWorldData["target"] = self.realWorldData["newTarget"]
            del self.realWorldData["newTarget"]
        self.saveWorld()
        return True  # whether the turn has been executed

    def HTMLColoredPlayer(self, s, ID):
        if ID == None:
            return '<FONT COLOR="%s">' % COLORS[1] + s + "</FONT>"
        color = self.world.players[ID].color
        color = [(hex(x))[-2:] for x in color]
        color = "".join(color)
        return '<FONT COLOR="%s">' % color + s + "</FONT>"

    def honorific(self, rank):
        if rank == 1:
            honorific = HONORIFICS[-1]
        else:
            honorific = HONORIFICS[int(rank ** 0.5 * len(HONORIFICS))]
        return honorific

    def playerHonorific(self, playerId):
        return self.honorific(self.world.players[playerId].rank)

    def playerFullName(self, playerId, colored=False):
        s = self.playerHonorific(playerId) + " " + self.world.players[playerId].name
        if colored:
            s = self.HTMLColoredPlayer(s, playerId)
        return s

    def __init__(self):
        # loading or creating world
        self.worldPath = os.path.join(
            os.path.dirname(os.path.realpath(__file__)), "World.sav"
        )
        try:
            self.loadWorld()
        except IOError:
            showInfo("AnkiStrategy: No saved game found. Starting a new game")
            self.world = GameWorld()
            self.realWorldData = {}
            startDate = datetime.date.today()
            self.realWorldData["startDate"] = startDate
            self.realWorldData["target"] = 100.0
            self.world.startGame(
                startDate.year * 10000 + startDate.month * 100 + startDate.day
            )
            self.saveWorld()
        # creating window
        self.mainWin = AnkiStrategyMainWindow(self, 1)
        menuItem = QAction("AnkiStrategy", mw)
        menuItem.triggered.connect(self.mainWin.show)
        mw.form.menuTools.addAction(menuItem)
        self.mapWindow = MapWindow(self.world)


if __name__ == "__main__":
    # independent application with text-only interface, mainly for testing
    w = GameWorld()
    w.startGame(6)
    print("location")
    location = input()
    w.players[1].location = w.cities[location]
    w.cities[location].playersInCity.append(w.players[1])
    print(w)
    for i in range(1000):
        print("insert work done:")
        workDone = float(input())
        w.execTurn(workDone)
        print("turn:", w.turn)
        print(w)
        # events
        if len(w.turnLog):
            print("events:", w.turnLog)
        w.setNewTurn()
        print("location")
        newLocation = input()
        if newLocation != w.players[1].location.name and newLocation != "":
            w.ordersForNextTurn.append(
                ("playermove", w.players[1].id, w.players[1].location.name, newLocation)
            )
        for j in w.newAmbassadors:
            location = w.players[j[0]].askIntelligence(w, "assignAmbassador")
            w.ordersForNextTurn.append(("ambassadorAssigned", j, location))
        w.newAmbassadors = []

        if i % 10 == 0:
            input()
        if max([x.rank for x in list(w.players.values())]) >= 100:
            break
