# -*- coding: UTF-8 -*-
# This file is part of AnkiStrategy. Copyright: 2013 <tcp-ip_80@libero.it>
# License: GNU AGPL, version 3 or later.
# This program is intended to be an add-on for Anki 2,
# note that Anki is copyrighted by Damien Elmes, who releases it under the same licence.
# You should have received a copy of the AGPL along with Anki. If not, see http://www.gnu.org/licenses/agpl.html
import random, datetime

# world generation constants
NPLAYERS = 10
NCITIES = 30
BIGGESTCITYSIZE = 3000000
WORLDSIZE = (1000, 600)
# game parameters
PEOPLECHANGINGOPINION = 0.10
PLAYERCLOUT = 1000
AMBASSADORCLOUT = 500
EXCHANGERATE = (
    10 ** -2
)  # 2 1 million inhabitants cities 1000Km away exchange 1% of their population
CITYGOVERNMENTPOWER = 0.02
MAXEXCHANGERATE = (
    0.02
)  # A city can't exchange more that this portion of inhabitants per turn
RETURNTONEUTRALRATE = 0.1
AMBASSADORLIFE = 90  # days
# interface
HONORIFICS = [
    "peasant",
    "baron",
    "viscount",
    "count",
    "marquis",
    "duke",
    "prince",
    "king",
    "emperor",
]
COLORS = [
    (255, 255, 255),
    (0, 0, 0),  # [0]=background color and [1]=neutral color
    (0, 255, 0),
    (255, 0, 0),
    (0, 0, 255),
    (200, 200, 0),
    (255, 0, 255),
    (0, 255, 255),
    (127, 255, 0),
    (255, 127, 0),
    (0, 127, 255),
    (200, 200, 127),
    (255, 127, 255),
    (127, 255, 255),
]


def randomRounding(f):
    i = int(f)
    r = f - i
    if random.random() < r:
        i += 1
    return i


class Player(object):
    def evalBestTargetCity(self, world):
        "AI evaluation function to find the best target"
        strongerEnemy = max(
            [(world.players[x].rank, world.players[x].id) for x in world.players]
        )
        values = {}
        for i in world.cities:
            if (
                self.id in world.cities[i].factions
                and world.cities[i].factions[self.id] > 0.5 * world.cities[i].size
            ):
                value = 0
            else:
                value = world.cities[i].factions.get(self.id, 0) / float(
                    world.cities[i].size
                )
                if strongerEnemy[1] in world.cities[i].government:
                    value += strongerEnemy[0]
            values[i] = value
        bestTargets = [x for x in values if values[x] == max(values.values())]
        return random.choice(bestTargets)

    def ai(self, world, question):
        if question == "location":
            return self.evalBestTargetCity(world)
        elif question == "assignAmbassador":
            return self.evalBestTargetCity(world)
        else:
            raise

    def askIntelligence(self, world, question):
        assert self.isAI
        return self.ai(world, question)

    def __init__(self, playerId, name, color):
        self.isAI = True
        self.id = playerId
        self.name = name
        self.color = color
        self.rank = 0
        self.location = None  # a city
        self.virtues = []  # list of virtues in all previous turns


class City(object):
    def localFactionChanges(self):
        "Exchange of citizens within a city"
        # The clout of each player in each city is calculated according to his virtue in this turn and other parameters:
        clout = {}
        # presence:  1000*virtue if the player is in the city,  500*virtue for each ambassador in the city.
        # Some citizens turns back to neutral.
        for i in self.playersInCity:
            clout[i.id] = clout.get(i.id, 0) + PLAYERCLOUT * i.virtues[-1]
        clout[None] = PLAYERCLOUT * RETURNTONEUTRALRATE
        for i in self.ambassadors:
            clout[i[0]] = (
                clout.get(i[0], 0)
                + AMBASSADORCLOUT * self.world.players[i[0]].virtues[-1]
            )
        # supporters: If the player already has supporters in the city, they can convince other people, expecially if the player has been virtuous: clout+=supporters*virtue
        # Neutral citizens can convince other people too, albeit with less influence.
        for i in self.factions:
            if i != None:
                clout[i] = (
                    clout.get(i, 0)
                    + self.world.players[i].virtues[-1] * self.factions[i]
                )
            else:
                clout[None] = (
                    clout.get(None, 0) + RETURNTONEUTRALRATE * self.factions[i]
                )
        # incumbency: Players that are part of the city government can use their power to shift public opinion.
        for i in self.government:
            if i != None:
                clout[i] = clout.get(i, 0) + self.world.players[i].virtues[
                    -1
                ] * self.size * CITYGOVERNMENTPOWER / len(self.government)
        # relative clout
        totalClout = float(sum(clout.values()))
        for i in clout:
            clout[i] = clout[i] / totalClout
        # Each turn 10% of the people in each faction (20% of the neutral citizens) becomes undecided and are reassigned to a player according to the relative clout
        # calculating undecided citizens
        undecided = 0
        for i in self.factions:
            changing = PEOPLECHANGINGOPINION * self.factions[i]
            if i == None:
                changing = min(self.size, changing * 2)
            changing = randomRounding(changing)
            undecided += changing
            self.factions[i] -= changing
        for i in clout:
            self.factions[i] = self.factions.get(i, 0) + randomRounding(
                clout[i] * undecided
            )

    def normalizeFactions(self):
        # rounding with consistent sum
        diff = sum(self.factions.values()) - self.size
        while diff != 0:
            t = random.choice(list(self.factions.keys()))
            new = self.factions[t] - abs(diff) / diff
            if new >= 0:
                self.factions[t] = new
                diff -= abs(diff) / diff
        # removing extinct factions
        extinctFactions = [x for x in self.factions if self.factions[x] == 0]
        for i in extinctFactions:
            del self.factions[i]
        # assert sum(self.factions.values()) == self.size#dl

    def electGovernment(self):
        "The players with the greatest faction in the city (summing up to at least 50%) are elected to the government"
        newGovernment = set()
        factionsList = [(self.factions[i], i) for i in self.factions]
        factionsList.sort(reverse=True)
        n = 0
        for i in factionsList:
            newGovernment.add(i[1])
            n += i[0]
            if n > self.size / 2:
                break
        if newGovernment != self.government:
            self.world.turnLog.append(
                (
                    "governmentChange",
                    self.name,
                    newGovernment - self.government,
                    self.government - newGovernment,
                )
            )
            self.government = newGovernment

    def __str__(self):
        return (
            self.name
            + "\t"
            + str(self.coordinates)
            + "\t"
            + str(self.size)
            + "\t"
            + str(self.government)
        )

    def __init__(self, world, coordinates, name, size):
        self.name = name
        self.world = world
        self.coordinates = coordinates
        self.size = size
        self.exchangeRates = {}
        self.factions = {}
        self.factions[None] = size  # neutral citizens
        self.playersInCity = []
        self.government = set((None,))
        self.ambassadors = []


class GameWorld(object):
    def __repr__(self):
        s = []
        cities = list(self.cities.values())
        cities.sort(key=lambda x: x.size, reverse=True)
        for i in cities:
            s.append(str(i))
            for j in i.playersInCity:
                s.append(" " + j.name)
            for j in i.ambassadors:
                s.append(" " + self.players[j[0]].name + "*")
            s.append(str(i.factions))
            s.append("\n")
        return "".join(s)

    def citiesDistance(self, a, b):
        return (
            (a.coordinates[0] - b.coordinates[0]) ** 2
            + (a.coordinates[1] - b.coordinates[1]) ** 2
        ) ** 0.5

    def calcCityExchangeRate(self):
        "Number of people moving form a city to another. It depends on their size and the distance between them"
        self.exchangeRates = {}
        citiesList = list(self.cities.values())
        for i in range(len(citiesList)):
            for j in range(i + 1, len(citiesList)):
                r = (
                    EXCHANGERATE
                    * float(citiesList[i].size)
                    * float(citiesList[j].size)
                    / (self.citiesDistance(citiesList[i], citiesList[j]) ** 2)
                )
                r = min(
                    r,
                    MAXEXCHANGERATE * citiesList[i].size,
                    MAXEXCHANGERATE * citiesList[j].size,
                )
                self.exchangeRates[(citiesList[i].name, citiesList[j].name)] = r
                citiesList[i].exchangeRates[citiesList[j].name] = r
                citiesList[j].exchangeRates[citiesList[i].name] = r

    def exchangesBetweenCities(self):
        links = list(self.exchangeRates.keys())
        random.shuffle(links)
        exchangedByCity = {i: 0 for i in self.cities}
        for l in links:
            # checking whether exchange is possible
            exchangeable1 = min(
                self.exchangeRates[l],
                self.cities[l[0]].size * MAXEXCHANGERATE - exchangedByCity[l[0]],
            )
            exchangeable2 = min(
                self.exchangeRates[l],
                self.cities[l[1]].size * MAXEXCHANGERATE - exchangedByCity[l[1]],
            )
            exchangeable = float(min(exchangeable1, exchangeable2))
            if exchangeable > 0:
                # leaving
                leaving = [{}, {}]
                relativeExchanges = (
                    exchangeable / self.cities[l[0]].size,
                    exchangeable / self.cities[l[1]].size,
                )
                for k in (0, 1):
                    for j in self.cities[l[k]].factions:
                        n = randomRounding(
                            relativeExchanges[k] * self.cities[l[k]].factions[j]
                        )
                        self.cities[l[k]].factions[j] -= n
                        leaving[k][j] = n
                    exchangedByCity[l[k]] += exchangeable
                # incoming
                for k in (0, 1):
                    for j in leaving[k]:
                        self.cities[l[1 - k]].factions[j] = (
                            self.cities[l[1 - k]].factions.get(j, 0) + leaving[k][j]
                        )

    def startGame(self, source):
        self.turn = 0
        self.turnLog = []
        # initializing players
        self.players[1] = Player(1, "Human", COLORS[2])
        self.players[1].isAI = False
        for i in range(2, NPLAYERS + 1):
            self.players[i] = Player(i, "AI" + str(i), COLORS[i + 1])
        # initializing world
        if type(source) == int:  # random cities
            r = random.Random()
            r.seed(source)
            usedCoordinates = set()
            for i in range(NCITIES):
                coordinates = (
                    r.randint(int(WORLDSIZE[0] * 0.05), int(WORLDSIZE[0] * 0.95)),
                    r.randint(int(WORLDSIZE[1] * 0.05), int(WORLDSIZE[1] * 0.95)),
                )
                while coordinates in usedCoordinates:
                    coordinates = (
                        r.randint(0, WORLDSIZE[0] - 1),
                        r.randint(0, WORLDSIZE[1] - 1),
                    )
                self.cities["city" + str(i)] = City(
                    self, coordinates, "city" + str(i), BIGGESTCITYSIZE / (i + 1)
                )
                usedCoordinates.add(coordinates)
            self.calcCityExchangeRate()
            self.WORLDSIZE = WORLDSIZE
        else:
            raise  # TODO (future release) load map from file
        # positioning players
        for i in self.players:
            if not self.players[i].isAI:
                continue
            location = self.players[i].askIntelligence(self, "location")
            self.players[i].location = self.cities[location]
            self.cities[location].playersInCity.append(self.players[i])

    def calcPlayerVirtues(self, workDone):
        # Human player's virtue is proportional to the square root of the percentage of the work he has done.
        # AI's virtues are random. The average is what the player will get if he does 95% of his work.
        for i in self.players.values():
            if i.isAI:
                i.virtues.append((random.random() * 1.9) ** 0.5)
            else:
                i.virtues.append(workDone ** 0.5)

    def appointNewAmbassadors(self):
        "A new ambassador is assigned if a player has been virtuous for a week"
        for i in self.players.values():
            if sum(i.virtues[-7:]) / 7 >= 0.95:
                self.newAmbassadors.append((i.id, self.turn + AMBASSADORLIFE))
                self.turnLog.append(("newAmbassador", self.newAmbassadors[-1]))
            else:
                self.turnLog.append(("noNewAmbassador", i.id, sum(i.virtues[-7:]) / 7))

    def removeOldAmbassadors(self):
        "Ambassadors retire after 90 days"
        for i in self.cities.values():
            while len(i.ambassadors) > 0 and i.ambassadors[0][1] == self.turn:
                self.turnLog.append(("oldAmbassadorRemoved", i.name, i.ambassadors[0]))
                del i.ambassadors[0]

    def execOrders(self):
        for i in self.ordersForNextTurn:
            self.turnLog.append(i)
            if i[0] == "playermove":
                player = self.players[i[1]]
                self.cities[i[2]].playersInCity.remove(player)
                self.cities[i[3]].playersInCity.append(player)
                player.location = self.cities[i[3]]
            elif i[0] == "ambassadorAssigned":
                self.cities[i[2]].ambassadors.append(i[1])
        self.ordersForNextTurn = []

    def filterEvents(self, keyType, key, eventsList):
        "Returns a sublist of a list with events or orders filtered according to the parameters"
        # possible events: ('newAmbassador', ambassador), ('noNewAmbassador', playerID, virtue),
        # ('governmentChange', city.name, newGovernment-oldgovernment, oldGovernment-newGovernment)
        # ('playerRankChange', i.id, int(i.rank*1000), int(playersRanks[i.id]*1000))
        # ('playermove', player.id, currentCity, newCity)
        # ('ambassadorAssigned', ambassador, location)
        # ('oldAmbassadorRemoved', 'location', ambassador)
        events = []
        for x in eventsList:
            if keyType == "player":
                if (
                    x[0]
                    in [
                        "newAmbassador",
                        "noNewAmbassador",
                        "playerRankChange",
                        "playermove",
                    ]
                    and x[1] == key
                ):
                    events.append(x)
                elif x[0] == "governmentChange" and (key in x[1] or key in x[2]):
                    events.append(x)
                elif x[0] == "ambassadorAssigned" and x[1][0] == key:
                    events.append(x)
            elif keyType == "city":
                if x[0] in ["newAmbassador", "noNewAmbassador", "playerRankChange"]:
                    continue
                elif x[0] == "governmentChange" and key == x[1]:
                    events.append(x)
                elif x[0] == "ambassadorAssigned" and key == x[2]:
                    events.append(x)
                elif x[0] == "playermove" and (key == x[2] or key == x[3]):
                    events.append(x)
            else:
                raise  # unknown keyType
        return events

    def execTurn(self, playerVirtue):
        self.turn += 1
        self.turnLog = []
        self.execOrders()
        self.calcPlayerVirtues(playerVirtue)
        # changes in the cities
        for i in self.cities.values():
            i.localFactionChanges()
        self.exchangesBetweenCities()
        for i in self.cities.values():
            i.normalizeFactions()
        for i in self.cities.values():
            i.electGovernment()
        # ambassadors
        if self.turn % 7 == 0:
            self.appointNewAmbassadors()
        self.removeOldAmbassadors()
        # player rank. It depends on the amount of cities in whose government he is.
        playersRanks = {x: 0 for x in self.players}
        for i in self.cities.values():
            for j in i.government:
                if j != None:
                    playersRanks[j] += 1.0 / len(i.government)
        playersRanks = {x: playersRanks[x] / len(self.cities) for x in playersRanks}
        for i in self.players.values():
            if i.rank != playersRanks[i.id]:
                self.turnLog.append(
                    ("playerRankChange", i.id, i.rank, playersRanks[i.id])
                )
            i.rank = playersRanks[i.id]

    def setNewTurn(self):
        # moving players and ambassadors
        for i in self.players.values():
            if not i.isAI:
                continue
            newLocation = i.askIntelligence(self, "location")
            if newLocation != i.location.name:
                self.ordersForNextTurn.append(
                    ("playermove", i.id, i.location.name, newLocation)
                )
        newAmbassadorsHuman = []
        for i in self.newAmbassadors:
            if not self.players[i[0]].isAI:
                newAmbassadorsHuman.append(i)
                continue
            location = self.players[i[0]].askIntelligence(self, "assignAmbassador")
            self.ordersForNextTurn.append(("ambassadorAssigned", i, location))
        self.newAmbassadors = newAmbassadorsHuman

    def __init__(self):
        self.players = {}
        self.cities = {}
        self.newAmbassadors = []
        self.ordersForNextTurn = []
